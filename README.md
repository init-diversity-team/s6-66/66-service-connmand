Build Instructions
------------------

# Requirements

- GNU make version 3.81 or later

This software will install on any operating system that implements POSIX.1-2008, available at [opengroup](http://pubs.opengroup.org/onlinepubs/9699919799/).


## Recommended build instructions

The recommended method to build this package directly from git.
```
gbp clone https://gitlab.com/init-diversity/s6-66/66-service-connmand.git && 
cd 66-service-connmand && 
gbp buildpackage -uc -us
``` 
The following should get you all the software required to build using this method:
```
sudo apt install git-buildpackage lowdown
```

Note: the man and html documentation pages will always be generated if *lowdown* is installed on your system. However, if you don't ask to build the documentation the final `DESTDIR` directory will do not contains any documentation at all.

## Customization

You can customize paths via flags given to configure. See `./configure --help` for a list of all available configure options.
These flags will need to be added to the debian/rules file.

## Runtime dependencies

- execline version 2.9.4.0 or later: http://skarnet.org/software/execline/
- s6 version 2.12.0.3 or later: http://skarnet.org/software/s6/
- 66 version 0.7.0.0 or later: https://git.obarun.org/Obarun/66/
- 66-tools version 0.1.0.0 or later: https://git.obarun.org/Obarun/66-tools/
- s6-linux-utils version 2.6.2.0 or later: http://skarnet.org/software/s6-linux-utils/
- s6-portable-utils version 2.3.0.3 or later: http://skarnet.org/software/s6-portable-utils/
